import React from 'react';
import Banner from './components/Banner';

export default function App() {
  return (
    <main>
      <section>
        <Banner />
      </section>
    </main>
  );
}
