import React, { Component } from 'react';
import css from './banner.css';

class Banner extends Component {
  state = {

  };

  render() {
    return (
      <div className={css.banner}>
        <h1 className={css.bannerTitle}>Fashion sale</h1>
        <button className={css.bannerBtn}>Check</button>
      </div>
    );
  }
}

export default Banner;
